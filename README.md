# TwitterList2Mastodon

## Why?

I'm not spending my time on Twitter these days. I prefer [Mastodon](https://joinmastodon.org/) because it's a decentralized, not a advertising driven, user spying platform. It's [free (as in speach, not as in beer)](https://en.wikipedia.org/wiki/Free_software). But some Twittos I like didn't made the switch (yet), so I made a list of them and using it to crosspost their tweets to my mastodon's timeline.

## Setup

- Clone this repository
- `composer install`
- Create a Twitter App (https://developer.twitter.com/en/apps)
- Create a new Mastodon bot locked account (/settings/profile)
- Create a Mastodon App (/settings/applications) from this new account
- Create a `config.php` from the `config.sample.php` sample and fill in the needed informations.

## How can I use it?

Use a [cronjob](https://en.wikipedia.org/wiki/Cron) to run `php index.php`. That's it. Don't be too greedy with the bandwitch of your Mastodon's instance. A call every 1/2h could be enough if you don't have a lot of talkative users on your list.

## TODO

- Twitter list API calls error handling
- Mastodon post calls error handling (tweet ids not posted are stored into an errors file but that's it at the moment)
- Mastodon API errors if user throttled handling
