<?php

$config = [
    'mastodon' => [
        'url' => '', // Your Mastodon instance URL eg. https://yourmastodoninstance.com
        'token' => '', // Get this token after creating a new Mastodon application (/settings/applications)
        'visibility' => '' // public, unlisted, private, direct
    ],

    'twitter' => [
        'consumer_key' => '', // Create a new Twitter application from https://developer.twitter.com/en/apps
        'consumer_secret' => '',
        'twitter_token' => '',
        'twitter_token_secret' => '',
        'list_id' => 0 // The list_id is the last URL part of the Twitter timeline list view
    ]
];
