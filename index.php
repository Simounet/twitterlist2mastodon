<?php

require "vendor/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

class Mastodon {

    private $config = [];

    public function __construct($config) {
        $this->config = $config;
    }

    public function post($text, $lang = 'en') {
        $status = [
            "status" => $text,
            "language" => $lang,
            "visibility" => $this->config['visibility']
        ];
        $headers = [
            'Authorization: Bearer ' . $this->config['token']
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->config['url'] . "/api/v1/statuses");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $status);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $curlResult = json_decode(curl_exec($curl));
        curl_close ($curl);
        return $curlResult;
    }
}

class Twitter {

    private $connection;
    private $config = [];

    public function __construct($config) {
        $this->config = $config;
        $this->connection = new TwitterOAuth($this->config['consumer_key'], $this->config['consumer_secret'], $this->config['twitter_token'], $this->config['twitter_token_secret']);
    }

    public function getChronologicalTweets($sinceId = false) {
        $params = [
            'list_id' => $this->config['list_id'],
            'include_entities' => false,
            'count' => 10
        ];
        if($sinceId) {
            $params['since_id'] = $sinceId;
        }
        $content = $this->connection->get('lists/statuses', $params);
        $chronologicalContent = array_reverse($content);
        return $chronologicalContent;
    }

    public function getListMembers() {
        return $this->connection->get('lists/members', [
            'list_id' => $this->config['list_id'],
            'include_entities' => false,
            'count' => 500
        ]);
    }
}

class Twitter2Mastodon {

    const ERRORS_FILE = 'errors';
    const SINCE_FILE = 'since';

    public function proceed() {
        require_once(__DIR__ . '/config.php');
        $sinceId = $this->getSinceId();
        $tweets = (new Twitter($config['twitter']))->getChronologicalTweets($sinceId);
        if(count($tweets) === 0) {
            echo "Take a break, you're going too fast!" . PHP_EOL;
            return false;
        }
        $this->saveSinceId(end($tweets)->id);
        foreach($tweets as $tweet) {
            $result = (new Mastodon($config['mastodon']))->post($this->tootFormatting($tweet), $tweet->lang);
            if(!$result->id || !$result->created_at) {
                file_put_contents(self::ERRORS_FILE, $tweet-id . "\n", FILE_APPEND);
            }
            sleep(1);
        }
    }

    private function tootFormatting($tweet) {
        return $this->usernamePrefix($tweet) . $this->tootBody($tweet) . ' ' . $this->tweetUrl($tweet);
    }

    private function usernamePrefix($tweet) {
        return '@' . $tweet->user->screen_name . ': ';
    }

    private function tootBody($tweet) {
        return isset($tweet->retweeted_status) ?
            'RT ' . $this->usernamePrefix($tweet->retweeted_status) . $tweet->retweeted_status->text : $tweet->text;
    }

    private function tweetUrl($tweet) {
        return 'https://twitter.com/' . $tweet->user->screen_name . '/statuses/' . $tweet->id;
    }

    private function getSinceId() {
        $lastSinceId = file_get_contents(self::SINCE_FILE);
        return $lastSinceId;
    }

    private function saveSinceId($id = '') {
       file_put_contents(self::SINCE_FILE, $id);
    }
}

(new Twitter2Mastodon)->proceed();
